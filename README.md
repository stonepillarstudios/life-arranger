# Life Arranger

This project houses a set of scripts I wrote to help keep my
life organised. Specifically, it does the following:

- Gets raw hourly weather data for the day, generates a useful graph, and
places it in a webserver dir for public accessibility.
- Depending on the time and day, crawls all over my Todoist tasks 
(using their API) and generates tables and lists with the most
important tasks I should be aware of. 
- Aggregates all this into a concise email and sends it to my personal 
inbox. 

## Artifacts

The following is a live preview of the weather graph the scripts generate.
Updated whenever the script runs. 

![Weather](https://libalex.athena.rauten.co.za/current_weather.png)
