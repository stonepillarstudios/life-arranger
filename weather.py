import requests
import matplotlib.pyplot as plt
import environ
import datetime
import json
import os
import time


def gen_weather():
    """Create an image tag with the current weather plotted"""

    hour_range = 12  # max 48 with the API I'm using

    try:
        weath = None
        if environ.PROD:
            weath = requests.get('https://weatherbit-v1-mashape.p.rapidapi.com/forecast/hourly',
                                 headers={
                                     'X-RapidAPI-Host': 'weatherbit-v1-mashape.p.rapidapi.com',
                                     'X-RapidAPI-Key': environ.WEATHERBIT_API_KEY
                                 },
                                 params={
                                     'lat': -33.925838,
                                     'lon': 18.42322
                                 }).json()['data']
        else:
            f = open('weath.json', 'r')
            weath = json.loads(f.read().replace('\n', ''))
            f.close()

        x_labs = []; y_temps = []; y_wind = []; y_rain = []
        last_m = ''
        for i in range(hour_range):
            hour = weath[i]

            h = datetime.datetime.fromtimestamp(hour['ts']).strftime("%-I%p")
            if last_m == h[-2:]:  # only specify AM/PM when changed.
                h = h[:-2]
            else:
                last_m = h[-2:]
            x_labs.append(h)

            y_temps.append( hour['temp'] )
            y_wind.append( hour['wind_spd'] )
            y_rain.append( hour['precip'] )

        # plot up!
        temp_col = 'red'; wind_col = 'lightblue'; rain_col = 'darkblue'

        fig, ax_rain = plt.subplots()

        b_rain = ax_rain.bar(x_labs, y_rain, color=rain_col, label='rain')
        ax_rain.set_yticklabels([])
        ax_rain.set_xlabel('time of day')
        ax_rain.set_ylim(0, 8)

        # label each bar
        for rect in b_rain:
            height = rect.get_height()
            if height > 0:
                if height > 7.5:  # on bar
                    ax_rain.text(rect.get_x() + rect.get_width() / 2., .98 * height,
                                 '%.2f mm/h' % height,
                                 ha='center', va='top', rotation=90, color='white')
                else:  # off bar
                    ax_rain.text(rect.get_x() + rect.get_width() / 2., 1.03 * height,
                                 '%.2f mm/h' % height,
                                 ha='center', va='bottom', rotation=90, color='black')


        ax_temp = ax_rain.twinx()

        ax_temp.set_ylabel('celcius', color=temp_col)
        ax_temp.yaxis.set_label_position('left')
        ax_temp.scatter(x_labs, y_temps, color=temp_col)
        l_temp, = ax_temp.plot(x_labs, y_temps, color=temp_col, label='temp')
        ax_temp.tick_params(axis='y', labelcolor=temp_col)
        ax_temp.set_ylim(5, 35)

        ax_wind = ax_temp.twinx()  # instantiate a second axis that shares the same x-axis

        ax_wind.set_ylabel('km/h', color=wind_col)
        ax_wind.yaxis.set_label_position('right')
        ax_wind.scatter(x_labs, y_wind, color=wind_col)
        l_wind, = ax_wind.plot(x_labs, y_wind, color=wind_col, label='wind')
        ax_wind.tick_params(axis='y', labelcolor=wind_col)
        ax_wind.set_ylim(0, 40)

        fig.legend([l_temp, l_wind, b_rain], ['temp', 'wind', 'rain'], loc=1, bbox_to_anchor=(.9, .9))
        plt.title('Weather for the next %d hours' % hour_range)

        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.savefig('current_weather.png')
        print('Weather graph generated')

        if environ.PROD:
            # move plot to public place on webserver
            os.rename('current_weather.png', environ.SERVE_PUBLIC_PATH + '/current_weather.png')

            return '<img src="' + environ.SERVE_PUBLIC_URL + '/current_weather.png?ts='+str(time.time())+'">'
            # adding on ts to ensure no mail client caching

        else:
            return '<img src="current_weather.png">'

    except Exception as e:
        return "<p>[Weather gen failed: %s]</p>" % e


if __name__ == '__main__':
    gen_weather()
