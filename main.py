import sys
import datetime
import emailer
import task_retriever
import weather


def morning_email():
    """Get tasks and compile morningly email"""

    html = task_retriever.styling()+"""
<body><p> Hey George. Here's your pre-day summary: </p>
<h2>%s</h2>""" % datetime.datetime.now().strftime("%A %-d %b")

    html += weather.gen_weather()

    html += "<h3>Tasks</h3>"

    taskstuff, taskcounts = task_retriever.morning()
    html += taskstuff

    html += "</body>"

    email_subject = 'Your day ahead: O%d F%d L%d W%d' % tuple(taskcounts)

    emailer.process_message(html, email_subject)


def evening_email():
    html = task_retriever.styling()+"""
<body><p>Hey, here's your evening summary: </p>
"""

    html += weather.gen_weather()

    html += "<h3>Tasks</h3>"

    taskstuff, taskcounts = task_retriever.evening()
    html += taskstuff

    html += "</body>"

    email_subject = "Evening summary: %d tasks left" % taskcounts[0]

    emailer.process_message(html, email_subject)


def weekend_email():

    html = task_retriever.styling()+"""
<body><p>Heya, here's your weekend info: </p>
<h3>Tasks</h3>
"""

    taskstuff, taskcounts = task_retriever.weekend()
    html += taskstuff

    html += '</body>'

    email_subject = "Weekend info: %d tasks to do" % taskcounts[0]

    emailer.process_message(html, email_subject)


if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("You need to specify an option: [morning, evening, weekend]")

    elif sys.argv[1] == 'morning':
        morning_email()

    elif sys.argv[1] == 'evening':
        evening_email()

    elif sys.argv[1] == 'weekend':
        weekend_email()

    else:
        print('unrecognised option: ' + sys.argv[1])
