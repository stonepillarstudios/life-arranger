import requests
import environ
import random
import datetime


# todo tasks completed yesterday/this week/earlier today
# todo more colours

# globals
Labels_Cache = None


def get_tasks(filter):
    for i in range(4):  # todoist api craps out sometimes
        rsp = requests.get(
            "https://api.todoist.com/rest/v1/tasks",
            params={"filter": filter},
            headers={
                "Authorization": "Bearer %s" % environ.TODOIST_API_TOKEN
            })
        if rsp.status_code == 200:
            return rsp.json()
    return None


def get_all_projs():
    raw = requests.get('https://api.todoist.com/rest/v1/projects',
                       headers={
                           "Authorization": "Bearer %s" % environ.TODOIST_API_TOKEN
                       }).json()

    proj_map = {}
    for proj in raw:
        proj_map[proj['id']] = proj['name']

    return proj_map


def get_all_labels():
    global Labels_Cache
    if Labels_Cache is not None:
        return Labels_Cache

    raw = requests.get('https://api.todoist.com/rest/v1/labels',
                       headers={
                           "Authorization": "Bearer %s" % environ.TODOIST_API_TOKEN
                       }).json()

    label_map = {}
    for lab in raw:
        label_map[lab['id']] = lab['name']

    Labels_Cache = label_map
    return label_map


def styling():
    return """
<style>
    #delivs td, #delivs th{
        border-right: 24px solid transparent;
    }
</style>
"""


def str_tasks(tasks, title, count_only=False, bgpresent='transparent'):
    tablepart = '<tr><th style="border-bottom: 1px solid black; border-top: 1px solid black;">%s</th></tr>' % title

    if tasks is None:
        tablepart += '<tr><td style="background-color: orange">&lt;Failed to retrieve info&gt;</td></tr>'
    elif len(tasks) > 0:
        if count_only:
            tablepart += '<tr><td style="background-color: %s">%d task%s</td></tr>' % \
                         (bgpresent, len(tasks), 's' if len(tasks) > 1 else '')
        else:
            for task in tasks:
                tablepart += '<tr><td style="background-color: %s">%s</td></tr>' % (bgpresent, task['content'])
    else:
        tablepart += '<tr><td style="background-color: lime">%s</td></tr>' % \
                     ['Nothing', 'All clear', 'No tasks', 'None'][random.randrange(3)]

    return tablepart


def upcoming_assignments():
    tbl = '<h4>Deliverables on the radar</h4> <table id="delivs">' \
          '<tr><th>Context</th><th>Desc</th><th>Time left</th><th>Labels</th></tr>'

    projs = get_all_projs()
    labs = get_all_labels()

    # special projects with due dates
    deliverables = get_tasks('!(#Inbox | #IndefiniteQueue) & (14 days)')

    # sort by date DESC
    deliverables.sort(key=lambda x: datetime.datetime.strptime(x['due']['date'], '%Y-%m-%d').date())

    if len(deliverables) > 0:
        for task in deliverables:
            desc = task['content']
            proj_context = projs[task['project_id']]
            due = datetime.datetime.strptime(task['due']['date'], '%Y-%m-%d').date()
            today = datetime.date.today()
            delta = (due - today).days
            labels = ';'.join([labs[lid] for lid in task['label_ids']])
            tbl += '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>' % \
                   (proj_context, desc,
                    '%d days' % delta if delta > 1 else 'tomorrow!' if delta == 1
                    else 'today!', labels
                    )
    else:
        tbl += '<tr><td style="background-color: lime"></td>' \
               '<td style="background-color: lime">Clear for now</td>' \
               '<td style="background-color: lime"></td><td style="background-color: lime"></td></tr>'

    return tbl + '</table>'


def indefinites():
    section = ''
    labs = get_all_labels()
    indef = get_tasks("#IndefiniteQueue & !@buy & !@debt")

    if len(indef) == 0:
        section += "<p>There are no indefinite queue items!</p>"
    else:
        section += "<p><u>Your random indefinite queue items for the day are:</u></p><ul>"

        random.shuffle(indef)
        i = 0
        while i < 3 and i < len(indef):
            section += "<li>%s %s</li>" % \
                       (indef[i]['content'],  # really complicated imperative code for putting labels in parentheses
                        '('+';'.join([labs[lid] for lid in indef[i]['label_ids']])+')'
                        if len(indef[i]['label_ids']) > 0 else '')
            i += 1

        section += "</ul>"

    return section


def morning():
    html = "<table>"

    html += str_tasks(get_tasks("today & @pre"), "Before you get busy")

    overdue = get_tasks("overdue")
    html += str_tasks(overdue, "Overdue", count_only=True, bgpresent='red')

    focus = get_tasks("today & (@eob | p1)")
    html += str_tasks(focus, "Focus up")

    later = get_tasks("today & !(@eob | p1 | @pre)")
    html += str_tasks(later, "Later today")

    weeklies = get_tasks("(@far | @eow) & (7 days)")
    html += str_tasks(weeklies[:3], "Some Weeklies")

    html += '</table>'

    html += upcoming_assignments()

    html += indefinites()

    return html, [len(overdue), len(focus), len(later), len(weeklies)]


def evening():
    html = '<table>'

    today = get_tasks("today")
    html += str_tasks(today, "Left today")

    html += str_tasks(get_tasks("(@far | @eow) & (7 days)"), "Weeklies")

    html += "</table>"

    html += upcoming_assignments()

    html += indefinites()

    return html, [len(today), ]


def weekend():
    html = '<table>'

    overdue = get_tasks('overdue')
    html += str_tasks(overdue, "Overdue")

    is_saturday = datetime.date.today().weekday() == 5

    this_weekend = get_tasks('today | tomorrow' if is_saturday else 'today')
    html += str_tasks(this_weekend, "This weekend")

    if not is_saturday:
        html += str_tasks(get_tasks('!today & (7 days)'), "Upcoming this week")

        html += str_tasks(get_tasks('@debt'), 'Debt')

    html += '</table>'

    html += upcoming_assignments()

    html += indefinites()

    return html, [len(this_weekend) + len(overdue), ]


if __name__ == '__main__':
    print(upcoming_assignments())
