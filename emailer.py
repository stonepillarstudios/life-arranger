import environ
from requests import post


def process_message(html, subject):
    if environ.PROD:

        resp = post('https://api.eu.mailgun.net/v3/rauten.co.za/messages',
                    data={
                        'from': environ.EMAIL_SELF,
                        'to': environ.EMAIL_GEORGE,
                        'subject': subject,
                        'html': html
                    },
                    auth=('api', environ.MAILGUN_API_KEY))

        print('Email send request:', resp.status_code)

    else:
        f = open('email.html', 'w')
        f.write(html)
        f.flush()
        f.close()
        print('Email generated. Subject:', subject)
